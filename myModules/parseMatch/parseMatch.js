var request = require("request-promise");
var config = require("config");

var parsedMatches = [];

async function parse(matchsData, playerID){
    parsedMatches = [];
    for(var i = 0; i < matchsData.data.length; i++){
        var match = matchsData.data[i];
        await parseMatch(match, matchsData, playerID);
    }
    return parsedMatches;
}

async function parseMatch(match, matchsData, playerID){
    var playerParticipant;
    var playerRoster;
    var enemyRoster;
    var asset;
    
    //Loop chaque roster du match
    match.relationships.rosters.data.forEach(r => {
        //recupere le roster dans les data
        var rosterData = matchsData.included.find(item => {
            return item.id == r.id;
        });
        
        var playerP;
        //Pour chaque participant
        rosterData.relationships.participants.data.forEach(participant => {
            if(!playerP){
                playerP =  matchsData.included.find(item => {
                    if(item.id == participant.id && item.relationships.player.data.id == playerID){
                        return true;
                    }
                });
            }
        });
        
        if(playerP){
            playerParticipant = playerP;
            playerRoster = rosterData;
        }else{
            enemyRoster = rosterData;
        }
    });
    
    asset = matchsData.included.find(item => {
        return item.id == match.relationships.assets.data[0].id;
    });
    
    var options = {
        uri: asset.attributes.URL,
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var parsedMatch = {};
    parsedMatch.playerParticipant = playerParticipant;
    parsedMatch.playerRoster = playerRoster;
    parsedMatch.enemyRoster = enemyRoster;
    var data = await request(options);
    parsedMatch.telemetryData = data;
    parsedMatches.push(parsedMatch);
}

module.exports = {parse}





        