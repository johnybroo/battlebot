var Canvas = require('canvas');
var rm = require("ressourcesManager");
var trads = require("traduction");

var divBorderColor = "#38516E";
var divBgColor = "#1F2D3F";
var textColor = "#9DB2C8";

var spacing = 5;

var UIElement = function(img){
    this.img = img;
    this.x = 0;
    this.y = 0;
    this.width = this.img.width;
    this.height = this.img.height;
}

UIElement.prototype.draw = function(ctx, x, y){
    this.x = x;
    this.y = y;
    ctx.drawImage(this.img, this.x, this.y);
}

UIElement.prototype.getBot = function(){return this.y + this.height};
UIElement.prototype.getRight = function(){return this.x + this.width};

async function createImage(teams, player, lang){
    var width1 = 600;
    var width2 = 160;
    var totalWidth = (width1+5)+(width2*3)+(spacing*2);
    
    var canvas = new Canvas(totalWidth, (130 + spacing) * teams.length + 40);
    var ctx = canvas.getContext('2d');
    
    var playerHeader = await imgHeader(player.attributes.name, totalWidth);
    playerHeader.draw(ctx, 0, 0);
    
    var lastElement;
    
    for(var i = 0; i < teams.length; i++){
        if(i == 0){
            var teamRanking = imgTeam(teams[i], width1, lang);
            teamRanking.draw(ctx, 0, playerHeader.getBot() + spacing);
            
            var teamWins = imgSimpleData(trads[lang].common.victories, teams[i].attributes.stats.wins, width2);
            teamWins.draw(ctx, teamRanking.getRight() + spacing, playerHeader.getBot() + spacing);
            
            var teamLosses = imgSimpleData(trads[lang].common.losses, teams[i].attributes.stats.losses, width2);
            teamLosses.draw(ctx, teamWins.getRight() + spacing, playerHeader.getBot() + spacing);
            
            var ratio = (teams[i].attributes.stats.wins / (teams[i].attributes.stats.wins + teams[i].attributes.stats.losses)) * 100;
            var teamRatio = imgSimpleData(trads[lang].common.ratio, (isNaN(ratio)) ? "-" : ratio.toFixed(1) + "%", width2);
            teamRatio.draw(ctx, teamLosses.getRight() + spacing, playerHeader.getBot() + spacing);
            
            lastElement = teamRatio;
        }else{
            var teamRanking = imgTeam(teams[i], width1, lang);
            teamRanking.draw(ctx, 0, lastElement.getBot() + spacing);
            
            var teamWins = imgSimpleData(trads[lang].common.victories, teams[i].attributes.stats.wins, width2);
            teamWins.draw(ctx, teamRanking.getRight() + spacing, lastElement.getBot() + spacing);
            
            var teamLosses = imgSimpleData(trads[lang].common.losses, teams[i].attributes.stats.losses, width2);
            teamLosses.draw(ctx, teamWins.getRight() + spacing, lastElement.getBot() + spacing);
            
            var ratio = (teams[i].attributes.stats.wins / (teams[i].attributes.stats.wins + teams[i].attributes.stats.losses)) * 100;
            var teamRatio = imgSimpleData(trads[lang].common.ratio, (isNaN(ratio)) ? "-" : ratio.toFixed(1) + "%", width2);
            teamRatio.draw(ctx, teamLosses.getRight() + spacing, lastElement.getBot() + spacing);
            
            lastElement = teamRatio;
        }
        
    }
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var footer = trads[lang].global.credits;
    var mt = ctx.measureText(footer);
    ctx.fillText(footer, (totalWidth/2)-(mt.width/2), (130 + spacing) * teams.length + 30 + 40);
    
    return canvas.toBuffer();
}

function imgHeader(text, width){
    return new Promise(function(resolve, reject){
        var height = 40;
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');
        
        ctx.fillStyle = divBgColor;
        ctx.fillRect(0,0, width,height);
    
        ctx.strokeStyle = divBorderColor;
        ctx.lineWidth = 5;
        ctx.strokeRect(0,0, width,height);
        
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var mt = ctx.measureText(text);
        ctx.fillText(text, (width/2)-(mt.width/2), (height/2) + 10);
        
        var img = new Canvas.Image();
        img.onload = () => {
            resolve(new UIElement(img));
        }
        img.src = canvas.toBuffer();
    });
}

function imgSimpleData(name, value, width){
    var height = 130;
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var mt = ctx.measureText(value);
    ctx.fillText(value, (width/2)-(mt.width/2), height/2);
    ctx.font = '20px Impact'
    mt = ctx.measureText(name);
    ctx.fillText(name, (width/2)-(mt.width/2), (height/2) + 20);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

function imgTeam(team, width, lang){
    var height = 130;
    
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    if(!team){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var txt = `${trads[lang].common.noTeam}`;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2)+15);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    if(team.attributes.stats.placementGamesLeft > 0){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var txt = `${trads[lang].modules.toPNG.placementGamesLeft} : ${team.attributes.stats.placementGamesLeft}`;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2)+15);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    var imgLeague = new Canvas.Image();
    imgLeague.src = rm.imgLeagues[team.attributes.stats.league];
    var offset = 110-imgLeague.width;
    ctx.drawImage(imgLeague, 0 + (offset/2), (height/2)-(imgLeague.height/2));
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var txt = team.attributes.name;
    var mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 37);
    
    txt = `${trads[lang].common.league} ${trads[lang].common.leagues[team.attributes.stats.league]}`;
    mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 77);
    
    txt = `${trads[lang].common.division} ${team.attributes.stats.division} (${team.attributes.stats.divisionRating})`;
    mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 117);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

module.exports = {createImage}