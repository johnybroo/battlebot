var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var statsSchema = new Schema({
    name : String,
    commands : {type : Number, default : 0}
});

module.exports = mongoose.model('Stats', statsSchema);