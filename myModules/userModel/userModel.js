var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    discordID : {type : String, required : true, unique : true},
    battleriteID : String,
    inviteCode : String
});

module.exports = mongoose.model('User', userSchema);