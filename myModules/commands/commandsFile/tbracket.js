var config = require("config");
var Command = require("command");
const fs = require("fs");
const {convert} = require('convert-svg-to-png');

var cmd = new Command("tbracket", execute, {category: "tournament", ownerOnly:true});

async function execute(message, args){
    //let body = request("https://challonge.com/34565456456.svg");
    let file = fs.readFileSync("./lol.svg");
    const png = await convert(file, {background: "black"});
    message.channel.send("LOL", {file: png})
}

module.exports = cmd;