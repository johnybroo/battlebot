var Command = require("command");
var trads = require("traduction");

var cmd = new Command("link", execute, {aliases:["l"], premium: true, category:"premium"});

async function execute(message, args){
    //----------------------- Verification des arguments ----------------------------------
    if(args.length < 1){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    global.tools.link(message, args[0]);
}

module.exports = cmd;