var fs = require("fs");
var path = require("path");

var tradsPath = path.join(__dirname, "traductions");
var trads = {};
fs.readdirSync(tradsPath).forEach(function(file) {
    var trad = JSON.parse(fs.readFileSync(`${tradsPath}/${file}`));
    trads[file.split(".")[0]] = trad;
});

module.exports = trads;