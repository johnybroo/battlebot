var Canvas = require('canvas');
var rm = require("ressourcesManager");
var trads = require("traduction");

var divBorderColor = "#38516E";
var divBgColor = "#1F2D3F";
var textColor = "#9DB2C8";

var spacing = 5;

var localTrad;
var commonTrad

var UIElement = function(img){
    this.img = img;
    this.x = 0;
    this.y = 0;
    this.width = this.img.width;
    this.height = this.img.height;
}

UIElement.prototype.draw = function(ctx, x, y){
    this.x = x;
    this.y = y;
    ctx.drawImage(this.img, this.x, this.y);
}

UIElement.prototype.getBot = function(){return this.y + this.height};
UIElement.prototype.getRight = function(){return this.x + this.width};

function createImage(player, topTeams, soloQ, lang){
    if(!lang){
        throw "Aucune langue spécifiée";
    }
    
    localTrad = trads[lang].modules.toPNG;
    commonTrad = trads[lang].common;
    
    var width1 = 600;
    var width2 = 160;
    var totalWidth = (width1+5)+(width2*3)+(spacing*2);
    
    var canvas = new Canvas(totalWidth, 800);
    var ctx = canvas.getContext('2d');
    
    var profile = imgProfile(player, width1, lang);
    profile.draw(ctx, 0, 0);
    
    var profileWins = imgSimpleData(commonTrad.victories, (player.attributes.stats.Wins) ? player.attributes.stats.Wins : "-", width2);
    profileWins.draw(ctx, profile.getRight() + spacing, 0);
    
    var profileLosses = imgSimpleData(commonTrad.losses, (player.attributes.stats.Losses) ? player.attributes.stats.Losses : "-", width2);
    profileLosses.draw(ctx, profileWins.getRight() + spacing, 0);
    
    var ratio = (player.attributes.stats.Wins / (player.attributes.stats.Wins + player.attributes.stats.Losses)) * 100;
    var profileRatio = imgSimpleData(commonTrad.ratio, (ratio) ? ratio.toFixed(1) + "%" : "-", width2);
    profileRatio.draw(ctx, profileLosses.getRight() + spacing, 0);
    //-----------------------------------------------------------------------------------
    var soloName = imgHeader(commonTrad.solo, totalWidth);
    soloName.draw(ctx, 0, profile.getBot() + spacing);
    
    var soloQAll = imgSoloQ(soloQ, width1);
    soloQAll.draw(ctx, 0, soloName.getBot() + spacing);
    
    var soloQWins = imgSimpleData(commonTrad.victories, (soloQ) ? soloQ.attributes.stats.wins : "-", width2);
    soloQWins.draw(ctx, soloQAll.getRight() + spacing, soloName.getBot() + spacing);
    
    var soloQLosses = imgSimpleData(commonTrad.losses, (soloQ) ? soloQ.attributes.stats.losses : "-", width2);
    soloQLosses.draw(ctx, soloQWins.getRight() + spacing, soloName.getBot() + spacing);
    
    var ratio = (soloQ) ? (soloQ.attributes.stats.wins / (soloQ.attributes.stats.wins + soloQ.attributes.stats.losses)) * 100 : null;
    var soloQRatio = imgSimpleData(commonTrad.ratio, (ratio) ? ratio.toFixed(1) + "%" : "-", width2);
    soloQRatio.draw(ctx, soloQLosses.getRight() + spacing, soloName.getBot() + spacing);
    //-----------------------------------------------------------------------------------
    var teamName = imgHeader(commonTrad.teams, totalWidth);
    teamName.draw(ctx, 0, soloQAll.getBot() + spacing);
    
    var team1 = imgTeam(topTeams[0], width1);
    team1.draw(ctx, 0, teamName.getBot() + spacing);
    
    var team1Wins = imgSimpleData(commonTrad.victories, (topTeams[0]) ? topTeams[0].attributes.stats.wins : "-", width2);
    team1Wins.draw(ctx, team1.getRight() + spacing, teamName.getBot() + spacing);
    
    var team1Losses = imgSimpleData(commonTrad.losses, (topTeams[0]) ? topTeams[0].attributes.stats.losses : "-", width2);
    team1Losses.draw(ctx, team1Wins.getRight() + spacing, teamName.getBot() + spacing);
    
    var ratio = (topTeams[0]) ? (topTeams[0].attributes.stats.wins / (topTeams[0].attributes.stats.wins + topTeams[0].attributes.stats.losses)) * 100 : null;
    var team1Ratio = imgSimpleData(commonTrad.ratio, (ratio) ? ratio.toFixed(1) + "%" : "-", width2);
    team1Ratio.draw(ctx, team1Losses.getRight() + spacing, teamName.getBot() + spacing);
    //------------------------------------------------------------------------------------
    var team2 = imgTeam(topTeams[1], width1);
    team2.draw(ctx, 0, team1.getBot() + spacing);
    
    var team2Wins = imgSimpleData(commonTrad.victories, (topTeams[1]) ? topTeams[1].attributes.stats.wins : "-", width2);
    team2Wins.draw(ctx, team2.getRight() + spacing, team1.getBot() + spacing);
    
    var team2Losses = imgSimpleData(commonTrad.losses, (topTeams[1]) ? topTeams[1].attributes.stats.losses : "-", width2);
    team2Losses.draw(ctx, team2Wins.getRight() + spacing, team1.getBot() + spacing);
    
    var ratio = (topTeams[1]) ? (topTeams[1].attributes.stats.wins / (topTeams[1].attributes.stats.wins + topTeams[1].attributes.stats.losses)) * 100 : null;
    var team2Ratio = imgSimpleData(commonTrad.ratio, (ratio) ? ratio.toFixed(1) + "%" : "-", width2);
    team2Ratio.draw(ctx, team2Losses.getRight() + spacing, team1.getBot() + spacing);
    //------------------------------------------------------------------------------------
    var team3 = imgTeam(topTeams[2], width1);
    team3.draw(ctx, 0, team2.getBot() + spacing);
    
    var team3Wins = imgSimpleData(commonTrad.victories, (topTeams[2]) ? topTeams[2].attributes.stats.wins : "-", width2);
    team3Wins.draw(ctx, team3.getRight() + spacing, team2.getBot() + spacing);
    
    var team3Losses = imgSimpleData(commonTrad.losses, (topTeams[2]) ? topTeams[2].attributes.stats.losses : "-", width2);
    team3Losses.draw(ctx, team3Wins.getRight() + spacing, team2.getBot() + spacing);
    
    var ratio = (topTeams[2]) ? (topTeams[2].attributes.stats.wins / (topTeams[2].attributes.stats.wins + topTeams[2].attributes.stats.losses)) * 100 : null;
    var team3Ratio = imgSimpleData(commonTrad.ratio, (ratio) ? ratio.toFixed(1) + "%" : "-", width2);
    team3Ratio.draw(ctx, team3Losses.getRight() + spacing, team2.getBot() + spacing);
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var footer = trads[lang].global.credits;
    var mt = ctx.measureText(footer);
    ctx.fillText(footer, (totalWidth/2)-(mt.width/2), 790);
    
    return canvas.toBuffer();
}

function imgHeader(text, width){
    var height = 40;
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var mt = ctx.measureText(text);
    ctx.fillText(text, (width/2)-(mt.width/2), (height/2) + 10);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

function imgSimpleData(name, value, width){
    var height = 130;
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var mt = ctx.measureText(value);
    ctx.fillText(value, (width/2)-(mt.width/2), height/2);
    ctx.font = '20px Impact'
    mt = ctx.measureText(name);
    ctx.fillText(name, (width/2)-(mt.width/2), (height/2) + 20);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

function imgProfile(player, width, lang){
    var height = 130;
    
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    var imgProfile = new Canvas.Image();
    imgProfile.src = rm.imgPictures[player.attributes.stats.picture];
    ctx.drawImage(imgProfile, 5, 5, height - 10, height - 10);
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var txt = player.attributes.name;
    ctx.fillText(txt, 140, 55);
    
    txt = rm.getTitle(player.attributes.stats.title, lang) || "";
    ctx.fillText(txt, 140, height - 30);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

function imgSoloQ(soloQ, width){
    var height = 130;
    
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    if(!soloQ){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        
        txt = localTrad.noRankedPlayed;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2) + 13);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    if(soloQ.attributes.stats.placementGamesLeft > 0){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        
        txt = `${localTrad.placementGamesLeft} : ${soloQ.attributes.stats.placementGamesLeft}`;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2) + 13);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    var imgLeague = new Canvas.Image();
    imgLeague.src = rm.imgLeagues[soloQ.attributes.stats.league];
    var offset = 110-imgLeague.width;
    ctx.drawImage(imgLeague, 0 + (offset/2), (height/2)-(imgLeague.height/2));
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var txt = `${commonTrad.league} ${commonTrad.leagues[soloQ.attributes.stats.league]}`;
    var mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 55);
    
    txt = `${commonTrad.division} ${soloQ.attributes.stats.division} (${soloQ.attributes.stats.divisionRating})`;
    mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, height - 30);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

function imgTeam(team, width){
    var height = 130;
    
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = divBgColor;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    if(!team){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var txt = localTrad.noTeam;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2)+15);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    if(team.attributes.stats.placementGamesLeft > 0){
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var txt = `${localTrad.placementGamesLeft} : ${team.attributes.stats.placementGamesLeft}`;
        mt = ctx.measureText(txt);
        ctx.fillText(txt, (width/2)-(mt.width/2), (height/2)+15);
        
        var img = new Canvas.Image();
        img.src = canvas.toBuffer();
        
        return new UIElement(img);
    }
    
    var imgLeague = new Canvas.Image();
    imgLeague.src = rm.imgLeagues[team.attributes.stats.league];
    var offset = 110-imgLeague.width;
    ctx.drawImage(imgLeague, 0 + (offset/2), (height/2)-(imgLeague.height/2));
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var txt = team.attributes.name;
    var mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 37);
    
    txt = `${commonTrad.league} ${commonTrad.leagues[team.attributes.stats.league]}`;
    mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 77);
    
    txt = `${commonTrad.division} ${team.attributes.stats.division} (${team.attributes.stats.divisionRating})`;
    mt = ctx.measureText(txt);
    ctx.fillText(txt, 110, 117);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

module.exports = {createImage}