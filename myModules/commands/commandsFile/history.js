var bs = require("battleriteService");
var pm = require("parseMatch");
var matchesToPNG = require("matchesToPNG");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("history", execute, {aliases:["h"], category: "stats"});

async function execute(message, args){
    if(args.length == 0){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    
    var trad = trads[message.lang].commands.history;
    if(args[1] && args[1] > 10){
        args[1] = 10;
    }
    
    var waitingMsgString = `${message.author}, ${trad.waitingStats}`;
    var waitingMsg = await message.channel.send(waitingMsgString);
    
    try{
        var player = await bs.getPlayersFromNames(args[0]);
        
        var matches = await bs.getLastMatches(player.id, message.lang, args[1]);
        
        waitingMsgString = `${message.author}, ${trad.waitingImage}`;
        waitingMsg.edit(waitingMsgString);
        
        var parsedMatches = await pm.parse(matches, player.id);
        
        var img = await matchesToPNG.createImage(parsedMatches, player, message.lang);
        
        waitingMsg.delete();
        message.channel.send({file: img});
    }catch(err){
        waitingMsg.delete();
        global.tools.handleErrorCommand(message, err);
        return;
    }
}

module.exports = cmd;