var Canvas = require('canvas');
var trads = require("traduction");

var divBorderColor = "#38516E";
var divBgColor = "#1F2D3F";
var textColor = "#9DB2C8";

var spacing = 5;

var UIElement = function(img){
    this.img = img;
    this.x = 0;
    this.y = 0;
    this.width = this.img.width;
    this.height = this.img.height;
}

UIElement.prototype.draw = function(ctx, x, y){
    this.x = x;
    this.y = y;
    ctx.drawImage(this.img, this.x, this.y);
}

UIElement.prototype.getBot = function(){return this.y + this.height};
UIElement.prototype.getRight = function(){return this.x + this.width};

async function createImage(player, lang = "en", sortIndex){
    if(player.attributes.stats.championArray.length == 0){
        throw "Ce joueur n'a pas fait de parties";
    }
    var trad = trads[lang].common;
    var width1 = 235;
    var width2 = 160;
    var totalWidth = (width1+5)+(width2*5)+(spacing*4);
    
    var canvas = new Canvas(totalWidth, 725+40);
    var ctx = canvas.getContext('2d');
    
    sortChampions(player.attributes.stats.championArray, sortIndex);
    player.attributes.stats.championArray = player.attributes.stats.championArray.slice(0, 5);
    
    var lastRowElement;
    
    var playerHeader = await imgHeader(player.attributes.name, totalWidth);
    playerHeader.draw(ctx, 0, 0);
    
    for (var i = 0; i < player.attributes.stats.championArray.length; i++) {
        var champ = player.attributes.stats.championArray[i];
        
        var champWins = champ.CharacterWins || 0;
        var champLosses = champ.CharacterLosses || 0;
        
        var ratio = (champWins / (champWins + champLosses)) * 100;
        
        var championImage = await imgChampion(champ);
        var winsImage = await imgSimpleData(trad.victories, champWins, width2);
        var lossesImage = await imgSimpleData(trad.losses, champLosses, width2);
        var ratioImage = await imgSimpleData(trad.ratio, ratio.toFixed(1) + "%", width2);
        var levelImage = await imgSimpleData(trad.level, champ.Level, width2);
        var timeImage = await imgSimpleData(trad.hours, Math.floor(champ.CharacterTimePlayed / 3600), width2);
        if(i == 0){
            championImage.draw(ctx, 0, playerHeader.getBot() + spacing);
            winsImage.draw(ctx, championImage.getRight() + spacing, playerHeader.getBot() + spacing);
            lossesImage.draw(ctx, winsImage.getRight() + spacing, playerHeader.getBot() + spacing);
            ratioImage.draw(ctx, lossesImage.getRight() + spacing, playerHeader.getBot() + spacing);
            levelImage.draw(ctx, ratioImage.getRight() + spacing, playerHeader.getBot() + spacing);
            timeImage.draw(ctx, levelImage.getRight() + spacing, playerHeader.getBot() + spacing);
            lastRowElement = timeImage;
        }else{
            championImage.draw(ctx, 0, lastRowElement.getBot() + spacing);
            winsImage.draw(ctx, championImage.getRight() + spacing, lastRowElement.getBot() + spacing);
            lossesImage.draw(ctx, winsImage.getRight() + spacing, lastRowElement.getBot() + spacing);
            ratioImage.draw(ctx, lossesImage.getRight() + spacing, lastRowElement.getBot() + spacing);
            levelImage.draw(ctx, ratioImage.getRight() + spacing, lastRowElement.getBot() + spacing);
            timeImage.draw(ctx, levelImage.getRight() + spacing, lastRowElement.getBot() + spacing);
            lastRowElement = timeImage;
        }
    }
    
    ctx.font = '30px Impact'
    ctx.fillStyle = textColor;
    var footer = trads[lang].global.credits;
    var mt = ctx.measureText(footer);
    ctx.fillText(footer, (totalWidth/2)-(mt.width/2), 755);
    
    return canvas.toBuffer();
}

function imgHeader(text, width){
    return new Promise(function(resolve, reject){
        var height = 40;
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');
        
        ctx.fillStyle = divBgColor;
        ctx.fillRect(0,0, width,height);
    
        ctx.strokeStyle = divBorderColor;
        ctx.lineWidth = 5;
        ctx.strokeRect(0,0, width,height);
        
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var mt = ctx.measureText(text);
        ctx.fillText(text, (width/2)-(mt.width/2), (height/2) + 10);
        
        var img = new Canvas.Image();
        img.onload = () => {
            resolve(new UIElement(img));
        }
        img.src = canvas.toBuffer();
    });
}

function imgSimpleData(name, value, width){
    return new Promise(function(resolve, reject){
        var height = 130;
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');
        
        ctx.fillStyle = divBgColor;
        ctx.fillRect(0,0, width,height);
    
        ctx.strokeStyle = divBorderColor;
        ctx.lineWidth = 5;
        ctx.strokeRect(0,0, width,height);
        
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var mt = ctx.measureText(value);
        ctx.fillText(value, (width/2)-(mt.width/2), height/2);
        ctx.font = '20px Impact'
        mt = ctx.measureText(name);
        ctx.fillText(name, (width/2)-(mt.width/2), (height/2) + 20);
        
        var img = new Canvas.Image();
        img.onload = () => {
            resolve(new UIElement(img));
        }
        img.src = canvas.toBuffer();
    });
}

function imgChampion(champion){
    return new Promise(function(resolve, reject){
        var height = 130;
        var width = 235;
        
        var imgWidth = 225;
        var imgHeight = 120;
        
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');
        
        ctx.fillStyle = divBgColor;
        ctx.fillRect(0,0, width,height);
    
        ctx.strokeStyle = divBorderColor;
        ctx.lineWidth = 5;
        ctx.strokeRect(0,0, width,height);
        
        var imgChamp = new Canvas.Image();
        imgChamp.onload = () => {
            ctx.drawImage(imgChamp, 5, 5, imgWidth, imgHeight);
        
            var img = new Canvas.Image();
            img.onload = () => {
                resolve(new UIElement(img));
            }
            img.src = canvas.toBuffer();
        }
        imgChamp.onerror = (err) => {
            console.log(err);
        }
        imgChamp.src = champion.WideIcon;
    });
}

function sortChampions(champions, sortIndex){
    switch (sortIndex) {
        case 2:
            champions.sort(function(a, b){
                var characterWinsA = a.CharacterWins || 0;
                var characterLossesA = a.CharacterLosses || 0;
                
                var characterWinsB = b.CharacterWins || 0;
                var characterLossesB = b.CharacterLosses || 0;
                
                var ratioA = (characterWinsA / (characterWinsA + characterLossesA)) * 100;
                var ratioB = (characterWinsB / (characterWinsB + characterLossesB)) * 100;
                
                if(ratioA > ratioB){
                    return -1;
                }
                return 1;
            });
            break;
        case 3:
            champions.sort(function(a, b){
                if(a.Level > b.Level){
                    return -1;
                }
                return 1;
            });
            break;
        case 0:
            champions.sort(function(a, b){
                var characterWinsA = a.CharacterWins || 0;
                var characterWinsB = b.CharacterWins || 0;
                
                if(characterWinsA > characterWinsB){
                    return -1;
                }
                return 1;
            });
            break;
        case 1:
            champions.sort(function(a, b){
                var characterLossesA = a.CharacterLosses || 0;
                var characterLossesB = b.CharacterLosses || 0;
                
                if(characterLossesA > characterLossesB){
                    return -1;
                }
                return 1;
            });
            break;
        default:
            champions.sort(function(a, b){
                if(a.CharacterTimePlayed > b.CharacterTimePlayed){
                    return -1;
                }
                return 1;
            });
    }
}

module.exports = {createImage}