var battleriteService = require("battleriteService");
var rm = require("ressourcesManager");
var statsToPNG = require("statsToPNG");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("stats", execute, {aliases:["s"], category:"stats"});

async function execute(message, args){
    if(args.length == 0){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    if(args[1]){
        args[1] = Number(args[1]) + 6;
    }
    
    var trad = trads[message.lang].commands.stats;
    var waitingMsgString = `${message.author}, ${trad.waitingStats}`;
    var waitingMsg = await message.channel.send(waitingMsgString);
    
    var player;
    var teams;
    try{
        player = await battleriteService.getPlayersFromNames(args[0], message.lang);
        rm.mapPlayer(player);
        teams = await battleriteService.getPlayerTeamsFromID(player.id, 3, message.lang, args[1]);
    }catch(err){
        console.log(err);
        global.tools.handleErrorCommand(message, err);
        waitingMsg.delete();
        return;
    }
    var statsImg;
    try{
        waitingMsgString = `${message.author}, ${trad.waitingImage}`;
        waitingMsg.edit(waitingMsgString);
        
        statsImg = statsToPNG.createImage(player, teams.teams, teams.solo, message.lang);
        
        message.channel.send({file: statsImg});
        waitingMsg.delete();
    }catch(err){
        waitingMsgString = `${message.author}, ${trad.errorImage}`;
        waitingMsg.edit(waitingMsgString);
        console.log(err);
    }
}

module.exports = cmd;