var config = require("config");
var Command = require("command");

var cmd = new Command("botStats", execute, {aliases:["bs"], ownerOnly : true});

function execute(message, args){
    if(message.author.id != config.ownerID){
        return;
    }
    let nbUser = 0;
    let nbGuilds = 0;
    let description = "";
    global.bot.guilds.forEach((guild) => {
        nbUser += guild.memberCount;
        nbGuilds++;
        description += `${guild.name} => ${guild.memberCount}\n`;
    });
    description += `${nbUser} utilisateurs pour ${nbGuilds} serveurs`;
    global.tools.handleSuccessCommand(message, description);
}

module.exports = cmd;