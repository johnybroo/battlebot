var path = require("path");
var sjson = require('simplified-json');
var fs = require('fs');
var iniParser = require("node-ini");
var config = require("config");

var imgLeagues = {};
var imgPictures = {};
var titles = {};
var characters = [];

var localization = {};

var stackables = JSON.parse(fs.readFileSync(path.join(__dirname, "assets", "map", "stackables.json")));
var gameplay = JSON.parse(fs.readFileSync(path.join(__dirname, "assets", "map", "gameplay.json")));

loadLocalization();
loadLeagueImages();
loadPicturesTitlesImages();
loadCharacters();

function loadLocalization(){
    var localizationPath = path.join(__dirname, "assets", "map", "localization");
    fs.readdirSync(localizationPath).forEach(function(file) {
        var localName;
        switch (file) {
            case 'French.ini':
                localName = "fr";
                break;
            case 'English.ini':
                localName = "en";
                break;
            case 'Spanish.ini':
                localName = "es";
                break;
            case 'Russian.ini':
                localName = "ru";
                break;
            case 'Brazilian.ini':
                localName = "pt";
                break;
            case 'Koreana.ini':
                localName = "kr";
                break;
            case 'German.ini':
                localName = "de";
                break;
            default:
                return;
        }
        var local = iniParser.parseSync(path.join(localizationPath, file));
        //if(localName === "en") console.log(local);
        localization[localName] = local;
    });
}

function loadLeagueImages(){
    for(var i = 0; i < 7; i++){
        var imgData = fs.readFileSync(path.join(__dirname, "assets", "img", "league", i + ".png"));
        imgLeagues[i] = imgData;
    }
}

function loadCharacters(){
    gameplay.characters.forEach(character => {
        var charObj = {};
        charObj.id = character.typeID;
        charObj.name = localization["en"][character.name];
        charObj.title = localization["en"][character.title];
        charObj.description = localization["en"][character.description];
        charObj.icon = getChampionImage(character.icon);
        charObj.wideIcon = getChampionImage(character.wideIcon);
        characters.push(charObj);
    });
}

function getTitle(titleID, lang = "en"){
    return localization[lang][titles[titleID]] || localization["en"][titles[titleID]];
}

function loadPicturesTitlesImages(){
    var picturesTitlesRaw = sjson.parse(fs.readFileSync(path.join(__dirname, "assets", "map", "AccountVanity.sjson")));
    
    for(var i = 0; i < picturesTitlesRaw.Titles.length; i++){
        titles[picturesTitlesRaw.Titles[i].StackableId] = picturesTitlesRaw.Titles[i].LocalizedText;
    }
    //--------------------------------------------------------------------------
    for(var i = 0; i < picturesTitlesRaw.Pictures.length; i++){
        try{
            imgPictures[picturesTitlesRaw.Pictures[i].StackableId] = fs.readFileSync(path.join(__dirname, "assets", "img", "avatars", picturesTitlesRaw.Pictures[i].Hash));
        }catch(err){
            imgPictures[picturesTitlesRaw.Pictures[i].StackableId] = fs.readFileSync(path.join(__dirname, "assets", "img", "avatars", "59331e63f782fb0577c46443e8b691bb"));
        }
    }
}

function getChampionImage(imgName){
    return fs.readFileSync(path.join(__dirname, "assets", "img", "game", imgName + ".png"));
}

//Map le joueur avec le fichier stackable.json
function mapPlayer(player){
    //On recupere que les stats du joueur
    var playerStats = player.attributes.stats;
    //On loop à travers tout les ID du fichier Stackable.json
    stackables.Mappings.forEach(mapItem => {
        //Si les stats du joueur ne contient pas le stackableID
        if(!playerStats.hasOwnProperty(mapItem.StackableId)){
            return;
        }
        //Si le stack a un nom traduit
        if(mapItem.LocalizedName){
            //Attention au fichier de traduction anglais, il se peut qu'il soit mal encodé et donc mal parsé
            playerStats[localization["en"][mapItem.LocalizedName]] = playerStats[localization["en"][mapItem.LocalizedName]] || {};
            playerStats[localization["en"][mapItem.LocalizedName]][mapItem.StackableRangeName] = playerStats[mapItem.StackableId];
            playerStats[localization["en"][mapItem.LocalizedName]].Icon = playerStats[localization["en"][mapItem.LocalizedName]].Icon || getChampionImage(mapItem.Icon);
            playerStats[localization["en"][mapItem.LocalizedName]].WideIcon = playerStats[localization["en"][mapItem.LocalizedName]].WideIcon || getChampionImage(mapItem.WideIcon);
        }else{
            
            playerStats[mapItem.DevName] = playerStats[mapItem.DevName] || {};
            playerStats[mapItem.DevName] = playerStats[mapItem.StackableId];
        }
        delete playerStats[mapItem.StackableId];
    });
    
    player.attributes.stats.championArray = [];
    for (var property in player.attributes.stats) {
        if (player.attributes.stats.hasOwnProperty(property)) {
            if(config.game.champions.includes(property)){
                player.attributes.stats[property].name = property;
                player.attributes.stats.championArray.push(player.attributes.stats[property]);
            }
        }
    }
}

module.exports = {
    imgLeagues,
    imgPictures,
    titles,
    mapPlayer,
    characters,
    getTitle
}