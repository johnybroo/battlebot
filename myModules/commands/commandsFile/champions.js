var rm = require("ressourcesManager");
var bs = require("battleriteService");
var championsToPNG = require("championsToPNG");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("champions", execute, {aliases:["c"], category: "stats"});

async function execute(message, args){
    var trad = trads[message.lang].commands.champions;
    if(args.length == 0){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    
    var waitingMsgString = `${message.author}, ${trad.waitingChamps}`;
    var waitingMsg = await message.channel.send(waitingMsgString);
    
    try{
        var player = await bs.getPlayersFromNames(args[0], message.lang);
        rm.mapPlayer(player);
        waitingMsgString = `${message.author}, ${trad.waitingImage}`;
        waitingMsg.edit(waitingMsgString);
        var championsImage = await championsToPNG.createImage(player, message.lang);
        waitingMsg.delete();
        message.channel.send({file: championsImage});
    }catch(err){
        console.log(err);
        waitingMsg.delete();
        global.tools.handleErrorCommand(message, err);
        return;
    }
}

module.exports = cmd;