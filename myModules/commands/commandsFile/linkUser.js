var Command = require("command");
var trads = require("traduction");

var cmd = new Command("linkuser", execute, {aliases:["lu"], premium: true, mod: true, category:"premium"});

async function execute(message, args){
    //----------------------- Verification des arguments ----------------------------------
    if(args.length < 2){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    if(message.mentions.users.size == 0){
        global.tools.handleErrorCommand(message, trads[message.lang].commands.linkuser.noMention, this.name);
        return;
    }
    await global.bot.fetchUser(message.mentions.users.first().id);
    var member = await message.guild.fetchMember(message.mentions.users.first().id);
    global.tools.link(message, args[1], member.id);
}

module.exports = cmd;