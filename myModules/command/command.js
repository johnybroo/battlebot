/*Liste des options
mod : Si la commande ne peut etre executée seulement par un admin ou le proprio du serveur
aliases : Un tableau de chaine de characteres pour les alias de la commande
*/
function Command(name, execute, options){
    if(!name || !execute){
        throw "No name or executable function provided for the new command";
    }
    if(typeof name != "string"){
        throw "The name of the command should be a String";
    }
    if(typeof execute != "function"){
        throw "The execute function of the command should be a Function";
    }
    this.name      = name.toLowerCase();
    this.execute   = execute;
    this.mod       = (options && options.mod) ? options.mod : false;
    this.aliases   = (options && options.aliases) ? options.aliases : [];
    this.premium   = (options && options.premium) ? options.premium : false;
    this.ownerOnly = (options && options.ownerOnly) ? options.ownerOnly : false;
    this.category  = (options && options.category) ? options.category : "misc";
}

module.exports = Command;