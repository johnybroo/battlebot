var config = require("config");
var Command = require("command");

var cmd = new Command("joinServer", execute, {aliases:["js"], ownerOnly : true});

async function execute(message, args){
    if(message.author.id != config.ownerID){
        return;
    }
    if(args.length == 0){
        global.tools.handleErrorCommand(message, "Manque le nom du serveur");
        return;
    }
    let guild = global.bot.guilds.find("name", args.join(" "));
    let randomChan;
    do {
         randomChan = guild.channels.random();
    } while (randomChan.type != "text");
    try{
        let invite = await randomChan.createInvite();
        message.reply(`https://discord.gg/${invite.code}`);
    }catch(err){
        message.reply("Impossible");
    }
    
}

module.exports = cmd;