var config = require("config");
var fs = require("fs");
var path = require("path");
var trads = require("traduction");
var Command = require("command");

var cmdPath = path.join(__dirname, "commandsFile");
var cmds = {};
fs.readdirSync(cmdPath).forEach(function(file) {
  var cmd = require("./commandsFile/" + file);
  cmds[cmd.name.toLowerCase()] = cmd;
});

var help = new Command("help", execute);

var commands = {help}

async function execute(message, args){
    //generateHelpEmbed(message);
    test(message);
}

function generateHelpEmbed(message){
    let trad = trads[message.lang].commands;
    let embed = global.tools.createEmbed(`:gear: ${trad.help.description}`, `${trad.help.longDescription}`, config.embed.color.success, message.lang);
    let cmdList = [];
    for(let cmd in commands){
        if(!commands.hasOwnProperty(cmd)) continue;
        if(commands[cmd].ownerOnly) continue;
        cmdList.push((commands[cmd].premium) ? message.dbGuild.commandSign + cmd + " (premium)" : message.dbGuild.commandSign + cmd);
    }
    let entier = Math.floor(cmdList.length / 3);
    let reste = cmdList.length % 3;
    
    for(let i = 0; i < 3; i++){
        let string = "";
        if(reste > 0){
            string = cmdList.splice(0, entier + 1).join("\n");
            reste--;
        }else{
            string = cmdList.splice(0, entier).join("\n");
        }
        embed.addField("\u200B", string, true);
    }
    embed.showIn(message.channel);
}

function test(message){
    let trad = trads[message.lang].commands;
    let embed = global.tools.createEmbed(`:gear: ${trad.help.description}`, `${trad.help.longDescription}`, config.embed.color.success, message.lang);
    let cmdList = {};
    
    for(let cmd in commands){
        if(!commands.hasOwnProperty(cmd)) continue;
        if(commands[cmd].ownerOnly) continue;
        let command = commands[cmd];
        if(!cmdList[command.category]) cmdList[command.category] = [];
        cmdList[command.category].push(message.dbGuild.commandSign + cmd);
    }
    
    for(let category in cmdList){
        if(!cmdList.hasOwnProperty(category)) continue;
        
        let entier = Math.floor(cmdList[category].length / 3);
        let reste = cmdList[category].length % 3;
        
        for(let i = 0; i < 3; i++){
            let string = "";
            if(reste > 0){
                string = cmdList[category].splice(0, entier + 1).join("\n");
                reste--;
            }else{
                string = cmdList[category].splice(0, entier).join("\n");
            }
            
            embed.addField((i == 0) ? trads[message.lang].common[category] : "\u200B", string || "\u200B", true);
        }
    }
    embed.showIn(message.channel);
}

module.exports = Object.assign(commands, cmds);