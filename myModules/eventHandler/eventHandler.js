var commands = require("commands");
var tools = require("tools");
var guildModel = require("guildModel");
var trads = require("traduction");

module.exports = {
    ready,
    message,
    guildCreate,
    guildDelete,
    roleDelete
}

async function ready(){
    console.log("Bot ready !");
    global.bot.user.setUsername("BattleBot").catch((err) => {console.log(err)});
    var ur = require("userRanking");
    ur.check();
}

async function message(message){
    //Ignore les messages provenant d'un bot ou si ce n'est pas un salon textuel
    if (message.author.bot || message.channel.type != "text") {
        return;
    }
    guildModel.findOne({guildID:message.guild.id}, function(err, guild){
        if(err){
            console.log(err.message || err);
            return;
        }
        if(!guild){
            var newGuild = new guildModel({guildID: message.guild.id});
            message.dbGuild = newGuild;
            newGuild.save(function(err, result){
                if(err){
                    console.log(err.message);
                    return;
                }
                console.log("Nouveau serveur enregistré : " + message.guild.name);
            });
        }else{
            message.dbGuild = guild;
        }
        
        if(message.content[0] == message.dbGuild.commandSign) {
            //Decoupage du message en commande et arguments
            var data = tools.parseMessage(message, message.dbGuild.commandSign);
            //On verifie si c'est bien une commande
            if(checkCmd(data)) {
                message.lang = message.dbGuild.lang || "en";
                message.commandSign = message.dbGuild.commandSign;
                if(!message.member){
                    message.guild.fetchMember(message.author)
                    .then((member) => {message.member = member; processCommand(message, data.cmd, data.args);})
                    .catch((err) => {console.log(err)});
                }else{
                    processCommand(message, data.cmd, data.args);
                }
            }
        }
    });
}

async function guildCreate(guild){
    var guildConfig = new guildModel({guildID: guild.id});
    guildConfig.save(function(err, result){
        if(err){
            console.log(err.message);
            return;
        }
        console.log("Nouveau serveur rejoin : " + guild.name);
    });
}

async function guildDelete(guild){
    guildModel.deleteOne({guildID: guild.id}, function(err, result){
        if(err){
            console.log("Erreur lors de la suppression d'un server quitté : " + err);
        }
        console.log("Serveur supprimé : " + guild.name);
    });
}

async function roleDelete(role){
    var dbGuild = await guildModel.findOne({guildID: role.guild.id}).exec();
    var deletedIDIndex = dbGuild.rankingRolesID.findIndex(id => {return id == role.id});
    if(deletedIDIndex == -1) return;
    var newRole = await role.guild.createRole({name: trads[dbGuild.lang].common.leagues[deletedIDIndex]});
    dbGuild.rankingRolesID.set(deletedIDIndex, newRole.id);
    
    dbGuild.save((err, newGuild) => {
        if(err){
            console.log(err);
            return;
        }
    });
}

//Verifie si c'est bien une commande ou un alias de commande
function checkCmd(data){
    for (var cmd in commands) {
        if (commands.hasOwnProperty(cmd)) {
            //Si la commande existe
            if(cmd == data.cmd){
                return true;
            }
            
            //Si l'alias existe
            if(commands[cmd].aliases.includes(data.cmd)){
                data.cmd = cmd;
                return true;
            }
        }
    }
    return false;
}

async function processCommand(message, cmd, args){
    var command = commands[cmd];
    console.log(`${command.name} : ${message.guild.name}`);
    //Si la guilde est pas premium et que la commande l'est
    if(message.dbGuild.premium == false && command.premium == true){
        global.tools.handleErrorCommand(message, trads[message.lang].global.noPremium);
        return;
    }
    
    if(!message.member.hasPermission("ADMINISTRATOR", false, true, true) && command.mod == true){
        global.tools.handleErrorCommand(message, trads[message.lang].global.noPermission);
        return;
    }
    
    //On verifie si c'est une commande d'aide
    if (args[0] == "help") {
        tools.handleHelpCommand(message, cmd);
    }
    else {
        //execution de la commande
        command.execute(message, args);
    }
}