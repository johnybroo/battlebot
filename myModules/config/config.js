var fs = require('fs');

var data = fs.readFileSync("./config.json");

try{
    var config = JSON.parse(data);
    console.log("Config loaded !");
}catch(err){
    console.log(err);
}

config.__proto__ = {
    save: function(){
        fs.writeFile("./config.json", JSON.stringify(config, null, 4), function(err){
            if(err){
                console.error(err);
                return;
            }
            console.log("Config saved !");
        });
    }
}

module.exports = config;