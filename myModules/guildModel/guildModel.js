var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var guildSchema = new Schema({
    guildID : {type : String, required : true, unique : true},
    premium : {type : Boolean, default : false},
    commandSign : {type : String, default : "!"},
    lang: {
        type: String,
        enum: ["en", "fr", "es", "kr", "pt", "ru", "de"],
        default: "en"
    },
    linkedMembers: [{type : Schema.Types.ObjectId, ref : "User"}],
    rankingRolesID: {type: [String], usePushEach: true},
    tournamentID: String
});

module.exports = mongoose.model('Guild', guildSchema);