var commands = require("commands");
var config = require("config");
var trads = require("traduction");
var guildModel = require("guildModel");
var bs = require("battleriteService");
var userModel = require("userModel");
var userRanking = require("userRanking");

//Liste des outils
var tools = {
    parseMessage: function (message, commandSign) {
        var str = message.content;
        if (str[0] != commandSign)
            return { cmd: null, args: [] };
        var data = str.split(commandSign);
        var cmd = data[1].split(/\s+/);
        var args = str.split(/\s+/);
        args.shift();
        return { cmd: cmd[0].toLowerCase(), args: args };
    },
    handleHelpCommand: function (message, cmd) {
        var trad = trads[message.lang].commands;
        var embed = this.createEmbed(`:grey_question: ${commands[cmd].name}`, trad[cmd].commandList || trad[cmd].description, config.embed.color.success, message.lang);
        if(commands[cmd].aliases.length > 0) embed.addField(trads[message.lang].common.aliases, commands[cmd].aliases.join(" "));
        embed.addField(trads[message.lang].common.help, "`" + message.commandSign + commands[cmd].name + " " + trad[cmd].help + "`");
        embed.showIn(message.channel);
    },
    handleErrorCommand: function (message, errorMsg, cmd) {
        var trad = trads[message.lang].commands;
        var embed = this.createEmbed(`:x: ${trads[message.lang].global.invalidCommand} !`, errorMsg, config.embed.color.error, message.lang);
        if(cmd){
            embed.addField(`${trads[message.lang].common.help} : `, "`" + message.commandSign + commands[cmd].name + " " + trad[cmd].help + "`");
        }
        embed.showIn(message.channel);
    },
    handleUnknowCommand: function (message) {
        var embed = this.createEmbed(`:x: ${trads[message.lang].common.error}`, `${trads[message.lang].global.commandNotFound}`, config.embed.color.error, message.lang);
        embed.addField(`${trads[message.lang].common.help} : `, "`" + message.commandSign + commands.help.help + "`");
        embed.showIn(message.channel);
    },
    handleSuccessCommand: function (message, successMsg){
        var embed = this.createEmbed(`:white_check_mark: ${trads[message.lang].common.success}`, successMsg, config.embed.color.success, message.lang);
        embed.showIn(message.channel);
    },
    createEmbed: function(title, description, color, lang = "en"){
        var trad = trads[lang];
        var embed = new global.discord.RichEmbed();
        embed.setTitle(title);
        embed.setDescription(description);
        embed.setFooter(trad.global.credits, config.embed.footer.image);
        embed.setColor(color);
        embed.showIn = function(channel){
            channel.send(this);
        }
        return embed;
    },
    createRankingRoles: async function(message){
        var roleNames = trads[message.lang].common.leagues;
        var newRolesArray = [];
        await createRoles();
        message.dbGuild.rankingRolesID = newRolesArray;
        await message.dbGuild.save();
        
        async function createRoles(){
            for(var roleName of roleNames){
                var role = await message.guild.createRole({name: roleName});
                newRolesArray.push(role.id);
            }
        }
    },
    renameRoles: async function(message){
        if(message.dbGuild.rankingRolesID.length != 7) return;
        var roleNames = trads[message.lang].common.leagues;
        
        renameRoles();
        
        function renameRoles(){
            for(var i = 0; i < roleNames.length; i++){
                message.guild.roles.get(message.dbGuild.rankingRolesID[i]).setName(roleNames[i]).catch((err) => {console.log("Erreur rename role : " + err)});
            }
        }
    },
    link: async function(message, playerName, memberID){
        if(message.dbGuild.rankingRolesID.length != 7){
            try{
                await global.tools.createRankingRoles(message);
                message.dbGuild = await guildModel.findOne({guildID:message.guild.id}).exec();
            }catch(err){
                console.log(err.message || err);
                if(err.code == 50013){
                    global.tools.handleErrorCommand(message, trads[message.lang].global.noRolesPermission);
                    return;
                }else{
                    global.tools.handleErrorCommand(message, trads[message.lang].global.internalError);
                    return;
                }
            }
        }
        //------------------------ Liement du joueur dans la base de données ---------------------------------------
        try{
            var player = await bs.getPlayersFromNames(playerName);
        }catch(err){
            global.tools.handleErrorCommand(message, err);
            return;
        }
        try{
            //Met à jour l'utilisateur dans la bdd ou l'ajoute si il n'existe pas
            await userModel.update({discordID: memberID || message.author.id}, {battleriteID: player.id}, {upsert: true, setDefaultsOnInsert: true}).exec();
            //Recupere l'utilisateur mis à jour
            var user = await userModel.findOne({discordID: memberID || message.author.id}).exec();
            //Ajoute l'utilisateur dans le tableau des utilisateurs lié du discord
            await guildModel.update({guildID: message.guild.id}, {$addToSet: {linkedMembers: user._id}}).exec();
            global.tools.handleSuccessCommand(message, trads[message.lang].commands.link.accountLinked);
        }catch(err){
            console.log(err);
            global.tools.handleErrorCommand(message, trads[message.lang].global.internalError);
            return;
        }
        //---------------------- Mise à jour des rôles du membre -----------------------------
        userRanking.check({dbGuild:message.dbGuild, linkedMember:user});
    },
    isMod: function(member){
        var memberRoles = member.roles;
        
        for(var i = 0; i < config.rolesID.botMods.length; i++){
            if(memberRoles.has(config.rolesID.botMods[i])){
                return true;
            }
        }
        return false;
    }
}

module.exports = tools;