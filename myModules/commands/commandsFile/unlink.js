var userModel = require("userModel");
var guildModel = require("guildModel");
var Command = require("command");
var trads = require("traduction");

var cmd = new Command("unlink", execute, {aliases:["ul"], premium: true, category:"premium"});

async function execute(message, args){
    try{
        //Recupere l'utilisateur
        var user = await userModel.findOne({discordID:message.author.id}).exec();
        //Retire l'utilisateur du tableau des utilisateurs lié du discord
        await guildModel.update({guildID: message.guild.id}, {$pull: {linkedMembers: user._id}}).exec();
        global.tools.handleSuccessCommand(message, trads[message.lang].commands.unlink.unlinkedAccount);
        message.member.removeRoles(message.dbGuild.rankingRolesID);
    }catch(err){
        console.log(err.message || err);
        if(err.code != 50013){
            global.tools.handleErrorCommand(message, trads[message.lang].global.internalError);
            return;
        }
    }
}

module.exports = cmd;