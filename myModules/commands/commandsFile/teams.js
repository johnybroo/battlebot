var brService = require("battleriteService");
var teamsToPNG = require("teamsToPNG");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("teams", execute, {aliases:["t"], category:"stats"});

async function execute(message, args){
    if(args.length == 0){
        global.tools.handleErrorCommand(message, trads[message.lang].global.missingArgs, this.name);
        return;
    }
    if(args[1]){
        args[1] = Number(args[1]) + 6;
        console.log(args[1])
    }
    var trad = trads[message.lang].commands.teams;
    
    var waitingMsgString = `${message.author}, ${trad.waitingStats}`;
    var waitingMsg = await message.channel.send(waitingMsgString);
    
    try{
        var player = await brService.getPlayersFromNames(args[0]);
        var teams = await brService.getPlayerTeamsFromID(player.id, 5, message.lang, args[1]);
        if(teams.teams.length == 0){
            waitingMsg.delete();
            global.tools.handleErrorCommand(message, trad.noTeams);
            return;
        }
        waitingMsgString = `${message.author}, ${trad.waitingImage}`;
        waitingMsg.edit(waitingMsgString);
        var image = await teamsToPNG.createImage(teams.teams, player, message.lang);
        message.channel.send({file: image});
        waitingMsg.delete();
    }catch(err){
        global.tools.handleErrorCommand(message, err);
        waitingMsg.delete();
        console.log(err);
        return;
    }
}

module.exports = cmd;