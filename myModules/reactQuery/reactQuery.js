const config = require("config");
const trads = require("traduction");

class ReactQuery{
    constructor(message){
        if(!message){
            throw new Error("Pas de message spécifié.");
        }
        this.authorID = message.author.id;
        this.channel = message.channel;
        this.choices = new Map();
        this.lang = message.dbGuild.lang;
    }
    
    addChoice(id, obj){
        this.choices.set(id, obj);
    }
    
    async send(ordered = false){
        let self = this;
        return new Promise(async function(resolve, reject){
            let description = trads[self.lang].modules.reactQuery.plzChoice;
        
            for(let [key, value] of self.choices){
                description += `${key} = ${global.tools.capitalizeFirstLetter(value.name)}\n`;
            }
            
            let embed = global.tools.createEmbed(":grey_question:", description, config.embed.color.success, self.lang);
            
            let newMessage = await self.channel.send(embed);
            
            let choices = self.choices;
            let authorID = self.authorID;
            
            let filter = function(reaction, user){
                return user.id == authorID && choices.has(reaction.emoji.name);
            }
            
            newMessage.awaitReactions(filter, { time: 30000, max : 1 })
            .then(collected => {
                if(collected.size > 0){
                    newMessage.delete();
                    resolve(self.choices.get(collected.firstKey()));
                }else{
                    newMessage.delete();
                    resolve(null);
                }
            });
            
            await newMessage.react(self.choices.entries().next().value[0]);
            
            if(ordered){
                try{
                    for(let [key, value] of self.choices){
                        if(self.choices.entries().next().value[0] == key) continue;
                        await newMessage.react(key);
                    }
                }catch(err){
                    
                }
            }else{
                for(let [key, value] of self.choices){
                    if(self.choices.entries().next().value[0] == key) continue;
                    newMessage.react(key).catch(err => {});
                }
            }
        });
    }
}

module.exports = ReactQuery;