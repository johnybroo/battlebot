var config = require("config");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("invite", execute);

async function execute(message, args){
    global.tools.handleSuccessCommand(message, config.invitationLink);
}

module.exports = cmd;