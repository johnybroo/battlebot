var Canvas = require('canvas');
var rm = require("ressourcesManager");
var trads = require("traduction");

var divBorderColor = "#38516E";
var divBgColor = "#1F2D3F";
var textColor = "#9DB2C8";
var loseColor = "#4F141B";

var width = 750;
var spacing = 5;

var UIElement = function(img){
    this.img = img;
    this.x = 0;
    this.y = 0;
    this.width = this.img.width;
    this.height = this.img.height;
}

UIElement.prototype.draw = function(ctx, x, y){
    this.x = x;
    this.y = y;
    ctx.drawImage(this.img, this.x, this.y);
}

UIElement.prototype.getBot = function(){return this.y + this.height};
UIElement.prototype.getRight = function(){return this.x + this.width};

async function createImage(matches, player, lang){
    var height = (154 * matches.length + spacing * matches.length - 1) + 40 + 40;
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    var lastMatchImg = {};
    
    var playerHeader = await imgHeader(player.attributes.name, width);
    playerHeader.draw(ctx, 0, 0);
    
    for(var i = 0; i < matches.length; i++){
        var matchImg = imgMatch(matches[i], lang);
        if(i == 0){
            matchImg.draw(ctx, 0, playerHeader.getBot() + spacing);
            lastMatchImg = matchImg;
        }else{
            matchImg.draw(ctx, 0, lastMatchImg.getBot() + spacing);
            lastMatchImg = matchImg;
        }
    }
    
    ctx.font = '30px Impact';
    ctx.fillStyle = textColor;
    var footer = trads[lang].global.credits;
    var mt = ctx.measureText(footer);
    ctx.fillText(footer, (width/2)-(mt.width/2), height - 10);
    
    return canvas.toBuffer();
}

function imgHeader(text, width){
    return new Promise(function(resolve, reject){
        var height = 40;
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');
        
        ctx.fillStyle = divBgColor;
        ctx.fillRect(0,0, width,height);
    
        ctx.strokeStyle = divBorderColor;
        ctx.lineWidth = 5;
        ctx.strokeRect(0,0, width,height);
        
        ctx.font = '30px Impact'
        ctx.fillStyle = textColor;
        var mt = ctx.measureText(text);
        ctx.fillText(text, (width/2)-(mt.width/2), (height/2) + 10);
        
        var img = new Canvas.Image();
        img.onload = () => {
            resolve(new UIElement(img));
        }
        img.src = canvas.toBuffer();
    });
}

function imgMatch(match, lang){
    var height = 154;
    var canvas = new Canvas(width, height);
    var ctx = canvas.getContext('2d');
    
    ctx.fillStyle = (match.playerRoster.attributes.won == "true") ? divBgColor : loseColor;;
    ctx.fillRect(0,0, width,height);

    ctx.strokeStyle = divBorderColor;
    ctx.lineWidth = 5;
    ctx.strokeRect(0,0, width,height);
    
    var userReservedMatch = match.telemetryData.find(item => {
        return item.type == "Structures.MatchReservedUser" && item.dataObject.accountId == match.playerParticipant.relationships.player.data.id;
    });
    
    var character = rm.characters.find(char => {
        return char.id == userReservedMatch.dataObject.character;
    });
    
    var champImg = new Canvas.Image();
    champImg.src = character.wideIcon;
    ctx.drawImage(champImg, 5,5);
    
    var teamSize, rankingType, winLose, score;
    
    switch (userReservedMatch.dataObject.serverType) {
        case 'QUICK3V3':
                teamSize = "3V3"
            break;
        case 'QUICK2V2':
                teamSize = "2V2"
            break;
        case 'PRIVATE':
                teamSize = trads[lang].modules.toPNG.privateGame;
            break;
        default:
            teamSize = userReservedMatch.dataObject.serverType;
            break;
    }
    
    rankingType = (userReservedMatch.dataObject.rankingType == "RANKED") ? trads[lang].common.ranked : trads[lang].common.unranked;
    winLose = (match.playerRoster.attributes.won == "true") ? trads[lang].common.victory : trads[lang].common.defeat;
    score = match.playerRoster.attributes.stats.score + " - " + match.enemyRoster.attributes.stats.score;
    
    ctx.font = '30px Impact';
    ctx.fillStyle = textColor;
    //2v2 ou 3v3
    var mt = ctx.measureText(teamSize);
    ctx.fillText(teamSize, champImg.width + 225 - mt.width / 2 - 100, 73);
    //ranked ou unranked
    var mt = ctx.measureText(rankingType);
    ctx.fillText(rankingType, champImg.width + 225 - mt.width / 2 - 100, 103);
    //victoire-defaite
    var mt = ctx.measureText(winLose);
    ctx.fillText(winLose, champImg.width + 400 - mt.width / 2 - 100, height / 2 + 10);
    //Score
    var mt = ctx.measureText(score);
    ctx.fillText(score, champImg.width + 520 - mt.width / 2 - 100, height / 2 + 10);
    
    var img = new Canvas.Image();
    img.src = canvas.toBuffer();
    
    return new UIElement(img);
}

module.exports = {createImage}