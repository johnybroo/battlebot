var config = require("config");
var request = require("request-promise");
var encodeurl = require("encodeurl");
var trads = require("traduction");

async function getPlayersFromNames(playerNames, lang = "en"){
    var options = {
        uri: `${config.apiURL}/players`,
        qs: {
            "filter[playerNames]": (playerNames.constructor === Array) ? encodeurl(playerNames.toString()) : encodeurl(playerNames)
        },
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var data = await makeRequest(options);
    if(!data || data.data.length == 0){
        throw trads[lang].global.playerNotFound;
    }
    data = data.data;
    return (playerNames.constructor === Array) ? data : data[0]
}

async function getPlayersFromIDs(playerIDs, lang = "en"){
    if(!playerIDs){
        throw "PlayerID undefined";
    }
    var options = {
        uri: `${config.apiURL}/players`,
        qs: {
            "filter[playerIds]": (playerIDs.constructor === Array) ? encodeurl(playerIDs.toString()) : encodeurl(playerIDs)
        },
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var data = await makeRequest(options);
    if(!data || data.data.length == 0){
        throw trads[lang].global.playerNotFound;
    }
    data = data.data;
    return (playerIDs.constructor === Array) ? data : data[0]
}

async function getPlayerTeamsFromID(playerID, count = 3, lang = "en", season = config.currentSeason){
    var options = {
        uri: `${config.apiURL}/teams`,
        qs: {
            "tag[season]" : season,
            "tag[playerIds]" : encodeurl(playerID)
        },
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var data = await makeRequest(options);
    
    if(!data || data.data.length == 0){
        throw trads[lang].global.noGamePlayedSeason;
    }
    data = data.data;
    var soloQ;
    
    for (var i = 0; i < data.length; i++) {
        var team = data[i];
        if(team.attributes.stats.members.length == 1){
            soloQ = team;
            if(season <= 4 && team.attributes.stats.league > 0){
                soloQ.attributes.stats.league--;
            }
            data.splice(i, 1);
        }
    }
    data.sort(function(a, b){
        var ligueA = a.attributes.stats.league;
        var divisionA = a.attributes.stats.division;
        var divisionRatingA = a.attributes.stats.divisionRating;
        
        var ligueB = b.attributes.stats.league;
        var divisionB = b.attributes.stats.division;
        var divisionRatingB = b.attributes.stats.divisionRating;
        
        if(ligueA > ligueB){
            return -1;
        }
        if(ligueA < ligueB){
            return 1;
        }
        if(ligueA == ligueB){
            if(divisionA > divisionB){
                return 1;
            }
            if(divisionA < divisionB){
                return -1;
            }
            if(divisionA == divisionB){
                if(divisionRatingA > divisionRatingB){
                    return -1;
                }
                if(divisionRatingA < divisionRatingB){
                    return 1;
                }
                return 0;
            }
        }
    });
    data = data.slice(0, count);
    
    var bulkTeamName = [];
    for(var i = 0; i < data.length; i++){
        var team = data[i];
        if(season <= 4 && team.attributes.stats.league > 0){
            team.attributes.stats.league--;
        }
        if(!team.attributes.name){
            bulkTeamName.push(setTeamName(team));
        }
    }
    
    await Promise.all(bulkTeamName);
    
    return {teams : data, solo : soloQ};
}

async function getPlayerHighestLeague(playerID, season = config.currentSeason){
    var options = {
        uri: `${config.apiURL}/teams`,
        qs: {
            "tag[season]" : season,
            "tag[playerIds]" : encodeurl(playerID)
        },
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var data = await makeRequest(options);
    if(!data || data.data.length == 0){
        return null;
    }
    data = data.data;
    
    data.sort(function(a, b){
        var ligueA = a.attributes.stats.league;
        var ligueB = b.attributes.stats.league;
        
        if(ligueA > ligueB){
            return -1;
        }
        if(ligueA < ligueB){
            return 1;
        }
        return 0;
    });
    
    if(data[0].attributes.stats.placementGamesLeft == 0){
        return data[0].attributes.stats.league;
    }
    return null;
}

async function getLastMatches(playerID, lang = "en", count = 5){
    var options = {
        uri: `${config.apiURL}/matches?filter[playerIds]=${playerID}&page[limit]=${count}&sort=-createdAt&sort=desc`,
        headers: {
            'Authorization': config.apiKey,
            "Accept": "application/vnd.api+json"
        },
        json: true
    };
    var data = await makeRequest(options);
    if(!data || data.length == 0){
        throw trads[lang].global.noRecentGames;
    }
    return data;
}

async function setTeamName(team){
    var teamPlayers = await getPlayersFromIDs(team.attributes.stats.members);
    var teamName = "";
    for(var j = 0; j < teamPlayers.length; j++){
        teamName += ((j == 0) ? "" : " & ") + teamPlayers[j].attributes.name;
    }
    team.attributes.name = teamName;
}

async function makeRequest(options){
    var data;
    try{
        data = await request(options);
        return data;
    }catch(err){
        return null;
    }
}

module.exports = {
    getPlayersFromNames,
    getPlayersFromIDs,
    getPlayerTeamsFromID,
    getPlayerHighestLeague,
    getLastMatches
}