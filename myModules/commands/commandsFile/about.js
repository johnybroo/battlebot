var config = require("config");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("about", execute, {aliases:["a"]});

function execute(message, args){
    var trad = trads[message.lang].commands.about;
    var embed = global.tools.createEmbed(trad.description, trad.about, config.embed.color.success, message.lang);
    embed.showIn(message.channel);
}

module.exports = cmd;