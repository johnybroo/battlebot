global.discord = require("discord.js");
global.bot = new global.discord.Client();
global.tools = require("tools");
var eventHandler = require("eventHandler");
var mongoose = require('mongoose');
var config = require("config");

mongoose.Promise = global.Promise;
mongoose.connect(config.dbPath);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connexion a la base de données réussie");
    global.bot.login((process.env.NODE_ENV == "development") ? config.devToken : config.prodToken).catch(err => {console.log(err)});
});

global.bot.on("error", function(error){
    console.log(error);
});

global.bot.on("ready", eventHandler.ready);
global.bot.on("message", eventHandler.message);
global.bot.on("roleDelete", eventHandler.roleDelete);
global.bot.on("guildCreate", eventHandler.guildCreate);
global.bot.on("guildDelete", eventHandler.guildDelete);