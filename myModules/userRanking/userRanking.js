var guildModel = require("guildModel");
var bs = require("battleriteService");

var maxUserCheck = 30;

setInterval(check, 3600000/2);

async function check(user){
    if(user){
        checkRoles([user]);
        return;
    }
    try{
        var linkedMembers = [];
        //Recup des guildes premium de la bdd
        var dbGuilds = await guildModel.find({premium: true}).populate("linkedMembers").exec();
        
        //Pour chaque guilde premium
        for(var dbGuild of dbGuilds){
            //Pour chaque membre lié de la guilde premium
            for(var linkedMember of dbGuild.linkedMembers){
                //Ajoute le membre lié avec la guilde de la base de données dans le tableau linkedMembers
                linkedMembers.push({dbGuild, linkedMember});
            }
        }
        
        var nbLoop = Math.floor(linkedMembers.length/maxUserCheck);
        if(linkedMembers.length % maxUserCheck) nbLoop++;
        
        for(var i = 0; i < nbLoop; i++){
            var start = i * maxUserCheck;
            var end = i * maxUserCheck + maxUserCheck;
            
            var newLinkedMembers = linkedMembers.slice(start, end);
            let newLinkedMembersCopy = newLinkedMembers.slice(0);
            
            setTimeout(() => {checkRoles(newLinkedMembersCopy)}, i*60000);
        }
    }catch(err){
        console.log("Erreur dans rankingRoles : " + err.message || err);
    }
}

async function checkRoles(linkedMembers){
    linkedMembers.forEach(async linkedUser => {
        var guild = await global.bot.guilds.get(linkedUser.dbGuild.guildID);
        
        try{
            await global.bot.fetchUser(linkedUser.linkedMember.discordID);
            var discordMember = await guild.fetchMember(linkedUser.linkedMember.discordID);
        }catch(err){
            //Retire l'utilisateur du tableau des utilisateurs lié du discord
            await guildModel.update({guildID: linkedUser.dbGuild.guildID}, {$pull: {linkedMembers: linkedUser.linkedMember._id}}).exec();
            return;
        }
        checkRankingRole(discordMember, linkedUser.linkedMember.battleriteID, linkedUser.dbGuild.rankingRolesID);
    })
}

async function checkRankingRole(discordMember, playerID, rolesID){
    var highestLeague = await bs.getPlayerHighestLeague(playerID);
    if(highestLeague == null){
        discordMember.removeRoles(rolesID).catch((err) => {});
        return;
    }
    var rankRoleID = rolesID[highestLeague];
    
    if(!discordMember.roles.has(rankRoleID)){
        updateRankingRole(discordMember, rankRoleID, rolesID);
    }
}

async function updateRankingRole(guildMember, newRoleID, rolesID){
    await guildMember.removeRoles(rolesID).catch((err) => {});
    guildMember.addRole(newRoleID).catch((err) => {});
}

module.exports = {
    check
}