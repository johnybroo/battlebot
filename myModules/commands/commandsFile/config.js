var config = require("config");
var guildModel = require("guildModel");
var trads = require("traduction");
var Command = require("command");

var cmd = new Command("config", execute, {mod:true});

async function execute(message, args){
    var trad = trads[message.lang].commands.config;
    var lang, commandSign;
    //--------------------- Choix de la langue --------------------------------------
    var embed = global.tools.createEmbed(`:gear: ${trad.botConfig}`, trad.whichLang, config.embed.color.success, message.lang);
    embed.addField("en", trads["en"].commands.config.chooseLang);
    embed.addField("fr", trads["fr"].commands.config.chooseLang);
    embed.addField("es", trads["es"].commands.config.chooseLang);
    embed.addField("ru", trads["ru"].commands.config.chooseLang);
    embed.addField("pt", trads["pt"].commands.config.chooseLang);
    embed.addField("kr", trads["kr"].commands.config.chooseLang);
    embed.addField("de", trads["de"].commands.config.chooseLang);
    await message.channel.send({embed});
    
    var filter = function(m){
        if(m.member.id != message.member.id){
            return false;
        }
        var filterTab = ["en","fr","es","ru","pt","kr","de"];
        return filterTab.includes(m.content.toLowerCase());
    }
    try{
        var collected = await message.channel.awaitMessages(filter, { maxMatches: 1, time: 30000, errors: ['time'] });
        lang = collected.first().content.toLowerCase();
        message.lang = lang;
        if(message.dbGuild.rankingRolesID.length == 7){
            global.tools.renameRoles(message);
        }
        guildModel.updateOne({guildID:message.guild.id}, {lang:lang}).exec();
        trad = trads[lang].commands.config;
    }catch(err){
        message.reply(trad.noResponse);
        return;
    }
    //-------------------- Choix du signe pour les commande -------------------------------
    embed = global.tools.createEmbed(`:gear: ${trad.botConfig}`, trad.whichSign, config.embed.color.success, lang);
    await message.channel.send({embed});
    
    filter = function(m){
        if(m.member.id != message.member.id){
            return false;
        }
        var char = m.content.charAt(0);
        var match = char.toLowerCase() != char.toUpperCase();
        return !match;
    }
    try{
        collected = await message.channel.awaitMessages(filter, { maxMatches: 1, time: 30000, errors: ['time'] });
        commandSign = collected.first().content.charAt(0);
        guildModel.updateOne({guildID:message.guild.id}, {"commandSign":commandSign}).exec();
        global.tools.handleSuccessCommand(message, trad.configDone, lang);
    }catch(err){
        message.reply(trad.noResponse);
        return;
    }
}

module.exports = cmd;